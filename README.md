
# Install

## Prerequisites

* a fresh Ubuntu 16.04 OS
* Internet connection
* Some familiarity with Linux, Docker, Python & JS, Django & Flask

## Prepare for installation

`sudo apt-get update`

Install git `sudo apt-get install git`

Install Docker CE, according to the [instructions](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce). You can verify if Docker is installed with `docker -v`.

Install Docker Compose according to the [instructions](https://docs.docker.com/compose/install/). You can verify if Docker Compose is installed with `docker-compose -v`.

Install means to create python virtualenv with `sudo apt-get install python3.5-venv`. Later, virtual environments are created with `python3.5 -m venv venv` in service folders. Venv is initiated with `source venv/bin/activate`, while the venv's python version can be verified with `python --version`. Don't forget to upgrade pip `pip install --upgrade pip` and install Python packages `pip install -r requirements.txt`.


## Install iconst, build and run constainers

```
mkdir iconst && cd iconst

git clone https://bitbucket.org/dborisog/iconst.git .

sudo docker-compose -f docker-compose-dev.yml build
sudo docker-compose -f docker-compose-dev.yml up -d
```

Instead of GeoNode,we install GeoServer in Docker containers provided in [kartoza/docker-geoserver](https://github.com/kartoza/docker-geoserver), as described in [his blogpost](http://kartoza.com/en/blog/orchestrating-geoserver-with-docker-and-fig/).

# Project structure


# Docker and docker-compose commands

build containers, for geonode and iconst

run docker and docker-compose with sudo

```
# Build the GeoNode images and run the containers:
docker-compose -f docker-compose-dev.yml build
docker-compose -f docker-compose-dev.yml up -d

# stop a set of containers:
docker-compose -f docker-compose.yml stop

# bring down a set of containers:
docker-compose -f docker-compose.yml down

# run the tests:
docker-compose -f docker-compose-dev.yml run visualilze python manage.py test

# delete all containers
docker rm $(docker ps -a -q)

# remove images:
docker rmi $(docker images -q)
```

# Upload maps from QGIS Desktop files

Login to GeoServer http://localhost/geoserver/ as admin:geoserver

Ensure existance of 'gis' workspace. 

Add new shapefiles 'stores' according according to [GeoServer documentation](https://geoserver.geo-solutions.it/edu/en/adding_data/add_shp.html), use non-capitalized names.

At the end you should be able to generate links similar to http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:testsite&outputFormat=application/json&srsname=EPSG:27700


# Project URLs

## GeoNode

http://localhost/ 

http://localhost/geoserver/  admin:geoserver

## Intelligent excavation

![microservices](./docs/microservices.png "Microservices")

| Microservice | Short description      | notable links               | port |
| ------------ | ---------------------- | --------------------------- | ---- |
| visualize    | Create GI 2D map       | / ; /api/log/               | 5005 |
| etrack       | Get excavator position | /etrack/api/position/       | 5010 |
| udetect      | Process GPR data       | /udetect/api/location/      | 5015 |
| wdetect      | Detect RFID worker     | /wdetect/api/people/        | 5020 |
| ireact       | Stop the excavator     | /ireact/api/rfid/ ; /ireact/api/overhead/ | 5025 |

# Versioning

Semantic Versioning 2.0.0 [specification](https://semver.org/spec/v2.0.0.html) is used for versioning. Versions can be found under the corresponsing branch, while master branch is recommended.

## master

This is a version to deploy. As soon as the new semantic version is ready, it is merged to the master.

## 0.1.0 -- ongoing

s1, s2, e1, e2, e3, p1, p2 scenarios are running on synthetic data
At the moment, this version is under development, any updates to this branch are intermediary.

## 0.0.0

s1, s2, e1, e2, e3, p1, p2 scenarios are running on synthetic data. Vector layers are hardcoded, scenario pages are separate to each other. This is a master branch version.

