# services/etrack/project/api/views.py
# views of GPS microservices

import datetime as dt
from flask import Blueprint, jsonify, render_template
from flask_cors import CORS
import os
from project.api import utils


etrack_blueprint = Blueprint('etrack', __name__)
CORS(etrack_blueprint)


SYNTHETIC_STEP = os.environ.get('synthetic_step')


@etrack_blueprint.route('/api/position/', methods=['GET'])
def position():
    # it is assumed an excavator is moving on cercle
    # with the radius of 37 meters, as it makes full
    # rotation moving with the velocity of 7 km/h

    initial = utils.initial_data(1.94444, 120, 480443.02, 238333.78)

    # process position and orientation for the current step
    tmp_dt = dt.datetime.now()
    time_now = utils.datetime2time(
        [tmp_dt.minute, tmp_dt.second, tmp_dt.microsecond])
    angle = utils.time2angle(initial, time_now)
    position = utils.angle2position(initial, angle)

    # process position and orentation for the previous step
    # TODO, replace constant with SYNTHETIC_STEP in unittesting
    if 'SYNTHETIC_STEP' not in vars() or 'SYNTHETIC_STEP' not in globals():
        SYNTHETIC_STEP = 257.1428571428572

    time_old = tmp_dt - dt.timedelta(
        milliseconds=SYNTHETIC_STEP)
    time_old = utils.datetime2time(
        [time_old.minute, time_old.second, time_old.microsecond])
    angle_old = utils.time2angle(initial, time_old)
    position_old = utils.angle2position(initial, angle_old)

    return jsonify({
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": position.get('coords')
        },
        "properties": {
            "bearing": position.get('vector'),
            "velocity": 7,
            "accuracy": 0.2,
            "previous": {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": position_old.get('coords')
                },
                "properties": {
                    "bearing": position_old.get('vector'),
                    "velocity": 7,
                    "accuracy": 0.2
                }
            }
        }
    })

# we might need a new function that takes the most recent value received
# from the store. where to store this value? Message queue?
# I don't want to store it in a file.
