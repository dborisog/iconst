# services/etrack/project/api/utils.py
# additional functions of etrack app

import math


def initial_data(velocity, period, x0, y0):
    """Return a dictionary of initial synthetic data.
    These are useful values for the simulation of an excavator
    moving in a circle.

    Input:
        velocity: in m/s, for example, 1.94444 from 7 km/h
        period: time period in seconds
        x0: x coordinate of the centre of the circle
        y0: y coordinate of the centre of the circle
    Output:
        output: a dictionary {velocity, tsegment, radius, x0, y0}
    """
    nr_min = int(period/60)
    nr_sec = period % nr_min

    output = {
        "velocity": velocity,
        'tsegment': [nr_min, nr_sec, 0],
        'radius': round((velocity * period) / (2 * math.pi), 5),
        'coords': [x0, y0]}

    return output


def datetime2time(timearray):
    """Normalise datatime.now() values into 120 second period.

    Input:
        timearray: [minutes, seconds, microserconds]
    Output:
        [minute, seconds, milliseconds] -- normalised to 120 periods
    """
    minute = timearray[0] % 2
    second = timearray[1]
    millisecond = int(timearray[2]/1000)
    return [minute, second, millisecond]


def time2angle(synthetic, timestamp):
    """Return angle in radians for the given time.

    Input:
        synthetic: dictionary of initial values
        timestamp: [minutes, seconds, milliseconds]
    Output:
        radian: an angle in radians
    """

    # time in milliseconds and velocity in m/ms
    time_value = timestamp[0] * 60000 + timestamp[1] * 1000 + timestamp[2]
    velocity_value = synthetic.get('velocity') / 1000

    radian = (time_value * velocity_value) / synthetic.get('radius')

    radian = round(radian, 5) % round((2 * math.pi), 5)

    return radian


def angle2position(synthetic, angle):
    """Return xy coordinates of an excavator depending on the initial
    data and the given angle in radians.

    Input:
        synthetic: dataset of initial values
        angle: angle in radians
    Output:
        output: {'coords': [x, y], 'vector': vector} -- coords and vector
    """

    x = (round(
            synthetic.get('coords')[0] +
            synthetic.get('radius') * math.sin(angle), 2))
    y = (round(
            synthetic.get('coords')[1] +
            synthetic.get('radius') * math.cos(angle), 2))
    vector = angle + math.pi / 2
    vector = round(vector, 5) % round((2 * math.pi), 5)
    return {'coords': [x, y], 'vector': vector}
