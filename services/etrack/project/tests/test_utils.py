# services/etrack/project/tests/test_utils.py

import math
from project.api import utils
from project.tests.base import BaseTestCase
import unittest


class TestUdetectUtils(BaseTestCase):
    """Test utils for etrack service."""

    def setUp(self):
        # prepare for tests
        self.synthetic = {
            "velocity": 1.94444,                 # m/s, from 7 km/h
            "tsegment": [2, 0, 0],            # time [m, s, ms]
            "radius": 37.13607,               # radius of the circle
            "coords": [480443.02, 238333.78]  # x coord, centre of the circle
        }

    def test_initial_data__set_sample__accurate(self):
        # test for initialization of synthetic data
        self.assertEqual(
            self.synthetic,
            utils.initial_data(1.94444, 120, 480443.02, 238333.78))

    def test_time2angle__scenarios__accurate(self):
        # test time2angle()
        self.assertEqual(0, utils.time2angle(self.synthetic, [0, 0, 0]))
        self.assertEqual(0, utils.time2angle(self.synthetic, [2, 0, 0]))
        self.assertEqual(
            round(math.pi, 5), utils.time2angle(self.synthetic, [0, 60, 0]))
        self.assertEqual(
            round(math.pi, 5), utils.time2angle(self.synthetic, [1, 0, 0]))

    def test_time2position__scenarios__accurate(self):
        # test time2position() function
        angle0 = 0
        angle90 = (math.pi / 2)
        angle180 = math.pi
        angle270 = 3 * math.pi / 2

        vector0 = round((math.pi / 2), 5)
        vector90 = round(math.pi, 5)
        vector180 = round((3 * math.pi / 2), 5)
        vector270 = 0

        position0 = [
            self.synthetic.get('coords')[0],
            round(
                self.synthetic.get('coords')[1] +
                self.synthetic.get('radius'), 2)]
        position90 = [
            round(
                self.synthetic.get('coords')[0] +
                self.synthetic.get('radius'), 2),
            self.synthetic.get('coords')[1]]
        position180 = [
            self.synthetic.get('coords')[0],
            round(
                self.synthetic.get('coords')[1] -
                self.synthetic.get('radius'), 2)]
        position270 = [
            round(
                self.synthetic.get('coords')[0] -
                self.synthetic.get('radius'), 2),
            self.synthetic.get('coords')[1]]

        self.assertEqual(
            {'coords': position0, 'vector': vector0},
            utils.angle2position(self.synthetic, angle0))
        self.assertEqual(
            {'coords': position90, 'vector': vector90},
            utils.angle2position(self.synthetic, angle90))
        self.assertEqual(
            {'coords': position180, 'vector': vector180},
            utils.angle2position(self.synthetic, angle180))
        self.assertEqual(
            {'coords': position270, 'vector': vector270},
            utils.angle2position(self.synthetic, angle270))

    def test_datetime2time__scenarios__accurate(self):
        # test datetime2timestamp()
        self.assertEqual([0, 0, 0], utils.datetime2time([0, 0, 0]))
        self.assertEqual([0, 60, 0], utils.datetime2time([0, 60, 0]))
        self.assertEqual([0, 60, 0], utils.datetime2time([2, 60, 0]))
        self.assertEqual([0, 0, 10], utils.datetime2time([0, 0, 10000]))


if __name__ == '__main__':
    unittest.main()
