# services/etrack/project/tests/test_views.py

from project.tests.base import BaseTestCase
from test.support import EnvironmentVarGuard
import unittest


class TestETrackService(BaseTestCase):
    """Tests for the etrack Service."""

    def test_position_json__GET__200status(self):
        # ensure the page exists
        self.env = EnvironmentVarGuard()
        self.env.set('synthetic_step', '257.1428571428572')

        with self.env:
            response = self.client.get('/api/position/')
            self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
