# services/visualize/project/__init__.py

from flask import Flask
import os


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    # register blueprints
    from project.api.visualize import visualize_blueprint
    app.register_blueprint(visualize_blueprint)

    # shell context for flask cli
    app.shell_context_processor({'app': app})
    return app
