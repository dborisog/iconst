# services/visualize/project/tests/test_visualize.py

from project.tests.base import BaseTestCase
import unittest


class TestVisualizeServiceWebLayer(BaseTestCase):
    """Tests web layer of Visualize service.

    Ensure if it responds to requests.
    """

    def test_index_page__GET__200status(self):
        # Ensure the route behaves correctly.
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_info_page__GET__200status(self):
        # Ensure the info page behaves correctly.
        response = self.client.get('/info/')
        self.assertEqual(response.status_code, 200)

    def test_h1_page__HEAD__200status(self):
        # Ensure the /h1/ route behaves correctly.
        response = self.client.head('/h1/')
        self.assertEqual(response.status_code, 200)

    def test_h2_page__HEAD__200status(self):
        # Ensure the /h2/ route behaves correctly.
        response = self.client.head('/h2/')
        self.assertEqual(response.status_code, 200)

    def test_e1_page__HEAD__200status(self):
        # Ensure the /e1/ route behaves correctly.
        response = self.client.head('/e1/')
        self.assertEqual(response.status_code, 200)

    def test_e2_page__HEAD__200status(self):
        # Ensure the /e2/ route behaves correctly.
        response = self.client.head('/e2/')
        self.assertEqual(response.status_code, 200)

    def test_e3_page__HEAD__200status(self):
        # Ensure the /e3/ route behaves correctly.
        response = self.client.head('/e3/')
        self.assertEqual(response.status_code, 200)

    def test_p1_page__HEAD__200status(self):
        # Ensure the /p1/ route behaves correctly.
        response = self.client.head('/p1/')
        self.assertEqual(response.status_code, 200)

    def test_p2_page__HEAD__200status(self):
        # Ensure the /p2/ route behaves correctly.
        response = self.client.head('/p2/')
        self.assertEqual(response.status_code, 200)

    def test_log_page__HEAD__200status(self):
        # Ensure the /p2/ route behaves correctly.
        response = self.client.head('/api/log/')
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
