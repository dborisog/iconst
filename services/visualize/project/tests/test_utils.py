# services/visualize/project/tests/test_utils.py

import copy
import os
import unittest
from unittest import mock

from project.api import utils
from project.tests.base import BaseTestCase


pointarray = [{
    "timevalue": 1521546257707, "reaction": {"stop": False},
    "position": {
        "properties": {"accuracy": 0.2, "bearing": 2.4695, "velocity": 7},
        "geometry": {"coordinates": [480472.08, 238356.9], "type": "Point"},
        "type": "Feature"}}, {

    "timevalue": 1521546258257, "reaction": {"stop": False},
    "position": {
        "properties": {"bearing": 2.498, "accuracy": 0.2, "velocity": 7},
        "geometry": {"coordinates": [480472.73, 238356.06], "type": "Point"},
        "type": "Feature"}}, {

    "timevalue": 1521546258786, "reaction": {"stop": False},
    "position": {
        "properties": {"accuracy": 0.2, "bearing": 2.527, "velocity": 7},
        "geometry": {"coordinates": [480473.36, 238355.19], "type": "Point"},
        "type": "Feature"}}, {

    "timevalue": 1521546259377, "reaction": {"stop": False},
    "position": {
        "properties": {"bearing": 2.5556, "accuracy": 0.2, "velocity": 7},
        "geometry": {"coordinates": [480473.96, 238354.32], "type": "Point"},
        "type": "Feature"}}, {

    "timevalue": 1521546259889, "reaction": {"stop": False},
    "position": {
        "properties": {"accuracy": 0.2, "bearing": 2.5851, "velocity": 7},
        "geometry": {"coordinates": [480474.55, 238353.4], "type": "Point"},
        "type": "Feature"}}]


# this methid will be used by the mock to replace requests.get
def mocker_requests_get_json(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    if args[0] == 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name')):
        return MockResponse({"type": "Feature"}, 200)

    return MockResponse(None, 404)


class TestVisualizeUtils(BaseTestCase):
    """Test transactional utils of visualize service."""

    @mock.patch('requests.get', side_effect=mocker_requests_get_json)
    def test_fetch_json__scenario__accurate(self, mock_get):
        # Test fetch_json.
        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        json_data = utils.fetch_json(utility_url)
        self.assertEqual(json_data, {"type": "Feature"})

    def test_ppDistance__scenario__accurate(self):
        # Test p2pDistance() distance between two points.
        self.assertEqual(1, utils.ppDistance([0, 0], [0, 1]))

    def test_multipointDistance__scenario__accurate(self):
        # Test multipointDistance() distance between two points.
        self.assertEqual(2, utils.multipointDistance([[0, 0], [0, 1], [1, 1]]))

    def test_positions2dv__scenario__accurate(self):
        # Test positions2dv() tranformation of GeoJSON to [[distance, vlocity]]

        output = [[1.06, 7], [1.07, 7], [1.06, 7], [1.09, 7]]
        self.assertEqual(output, utils.positions2dv(pointarray))

    def test_positions2tv__scenario__accurate(self):
        # Test positions2dv() tranformation of GeoJSON to [[distance, vlocity]]

        output = [[0.55, 7], [0.529, 7], [0.591, 7], [0.512, 7]]
        self.assertEqual(output, utils.positions2tv(pointarray))

    def test_sumCategories__scenario__accurate(self):
        # test SumCategories

        in_list = [[2.04, 7], [1.97, 7], [1.98, 7], [23.58, 7], [0.32, 7]]

        out_list = [[29.89, '[6, 8)']]
        out_zeros = [[0.0, 0.0]]

        dv_list, dv_zeros = utils.sumCategories(
            in_list, ['distance', 'velocity'], 2)

        self.assertEqual(out_list, dv_list)
        self.assertEqual(out_zeros, dv_zeros)

    def test_filterByStopEvent__scenario__accurate(self):
        # test filterByStopEvent()

        info = copy.deepcopy(pointarray)
        info[0]['reaction'] = {"stop": True}

        stops = [{
            "timevalue": 1521546257707, "reaction": {"stop": True},
            "position": {
                "properties": {
                    "accuracy": 0.2, "bearing": 2.4695, "velocity": 7},
                "geometry": {
                    "coordinates": [480472.08, 238356.9], "type": "Point"},
                "type": "Feature"}}, ]

        self.assertEqual(
            stops,
            utils.filterByStopEvent(info))


if __name__ == '__main__':
    unittest.main()
