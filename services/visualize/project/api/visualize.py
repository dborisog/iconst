# services/visualize/project/api/visualize.py

from datetime import datetime
from flask import Blueprint, current_app, jsonify, render_template, request
import functools
import json
import os
import pandas as pd
import project.api.utils as utils
import requests
import httplib2
from shapely import geometry
from werkzeug.contrib.cache import SimpleCache


visualize_blueprint = Blueprint(
    'visualize',
    __name__,
    template_folder='templates',
    static_folder='static',
    static_url_path='/static/')


@visualize_blueprint.app_template_filter()
def dtfilter(timestamp):
    return datetime.fromtimestamp(timestamp / 1000.0)


SYNTHETIC_STEP = os.environ.get('synthetic_step')


@visualize_blueprint.route('/')
def index():
    return render_template('index.html')


@visualize_blueprint.route('/info/')
def info():
    return render_template('info.html')


@visualize_blueprint.route('/h1/', methods=['GET', 'HEAD'])
def h1():
    """H1, Emergency stop when an excavator RFID sensor detects an RFID tag."""

    if request.method == 'GET':
        cache_obj = SimpleCache()

        area_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('area_gis_name'))

        if not cache_obj.get(area_url):
            area_json = utils.fetch_json(area_url)
            cache_obj.set(area_url, area_json, timeout=60 * 15)
        else:
            area_json = cache_obj.get(area_url)

        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        if not cache_obj.get(utility_url):
            utility_json = utils.fetch_json(utility_url)
            cache_obj.set(utility_url, utility_json, timeout=60 * 15)
        else:
            utility_json = cache_obj.get(utility_url)

        return render_template(
            'oper_h1.html',
            area_json=json.dumps(area_json),
            utility_json=json.dumps(utility_json),
            SYNTHETIC_STEP=SYNTHETIC_STEP)

    if request.method == 'HEAD':
        return 'OK'


@visualize_blueprint.route('/h2/', methods=['GET', 'HEAD'])
def h2():
    """H2, Emergency stop when the excavator is within a
    predetermined range/exclusion zone of an overhead cable."""

    if request.method == 'GET':
        cache_obj = SimpleCache()

        area_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('area_gis_name'))
        if not cache_obj.get(area_url):
            area_json = utils.fetch_json(area_url)
            cache_obj.set(area_url, area_json, timeout=60 * 15)
        else:
            area_json = cache_obj.get(area_url)

        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        if not cache_obj.get(utility_url):
            utility_json = utils.fetch_json(utility_url)
            cache_obj.set(utility_url, utility_json, timeout=60 * 15)
        else:
            utility_json = cache_obj.get(utility_url)

        return render_template(
            'oper_h2.html',
            area_json=json.dumps(area_json),
            utility_json=json.dumps(utility_json),
            SYNTHETIC_STEP=SYNTHETIC_STEP)

    if request.method == 'HEAD':
        return 'OK'


@visualize_blueprint.route('/e1/', methods=['GET', 'HEAD'])
def e1():
    """E1, Locate the excavator directly over the buried utility."""

    if request.method == 'GET':
        cache_obj = SimpleCache()

        area_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('area_gis_name'))
        if not cache_obj.get(area_url):
            area_json = utils.fetch_json(area_url)
            cache_obj.set(area_url, area_json, timeout=60 * 15)
        else:
            area_json = cache_obj.get(area_url)

        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        if not cache_obj.get(utility_url):
            utility_json = utils.fetch_json(utility_url)
            cache_obj.set(utility_url, utility_json, timeout=60 * 15)
        else:
            utility_json = cache_obj.get(utility_url)

        print("\n=======================")
        print(SYNTHETIC_STEP)
        print("=======================\n")

        return render_template(
            'oper_e1.html',
            area_json=json.dumps(area_json),
            utility_json=json.dumps(utility_json),
            SYNTHETIC_STEP=SYNTHETIC_STEP)

    if request.method == 'HEAD':
        return 'OK'


@visualize_blueprint.route('/e2/', methods=['GET', 'HEAD'])
def e2():
    """E2, Locate the excavator directly over the buried utility."""

    if request.method == 'GET':
        cache_obj = SimpleCache()

        area_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('area_gis_name'))
        if not cache_obj.get(area_url):
            area_json = utils.fetch_json(area_url)
            cache_obj.set(area_url, area_json, timeout=60 * 15)
        else:
            area_json = cache_obj.get(area_url)

        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        if not cache_obj.get(utility_url):
            utility_json = utils.fetch_json(utility_url)
            cache_obj.set(utility_url, utility_json, timeout=60 * 15)
        else:
            utility_json = cache_obj.get(utility_url)

        return render_template(
            'oper_e2.html',
            area_json=json.dumps(area_json),
            utility_json=json.dumps(utility_json),
            SYNTHETIC_STEP=SYNTHETIC_STEP)

    if request.method == 'HEAD':
        return 'OK'


@visualize_blueprint.route('/e3/', methods=['GET', 'HEAD'])
def e3():
    """E3, Locate the excavator parallel to the buried utility."""

    if request.method == 'GET':
        cache_obj = SimpleCache()

        area_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('area_gis_name'))
        if not cache_obj.get(area_url):
            area_json = utils.fetch_json(area_url)
            cache_obj.set(area_url, area_json, timeout=60 * 15)
        else:
            area_json = cache_obj.get(area_url)

        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        if not cache_obj.get(utility_url):
            utility_json = utils.fetch_json(utility_url)
            cache_obj.set(utility_url, utility_json, timeout=60 * 15)
        else:
            utility_json = cache_obj.get(utility_url)

        return render_template(
            'oper_e3.html',
            area_json=json.dumps(area_json),
            utility_json=json.dumps(utility_json),
            SYNTHETIC_STEP=SYNTHETIC_STEP)

    if request.method == 'HEAD':
        return 'OK'


@visualize_blueprint.route('/p1/', methods=['GET', 'HEAD'])
def p1():
    """P1, save map."""

    if request.method == 'GET':
        cache_obj = SimpleCache()

        area_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('area_gis_name'))
        if not cache_obj.get(area_url):
            area_json = utils.fetch_json(area_url)
            cache_obj.set(area_url, area_json, timeout=60 * 15)
        else:
            area_json = cache_obj.get(area_url)

        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        if not cache_obj.get(utility_url):
            utility_json = utils.fetch_json(utility_url)
            cache_obj.set(utility_url, utility_json, timeout=60 * 15)
        else:
            utility_json = cache_obj.get(utility_url)

        return render_template(
            'oper_p1.html',
            area_json=json.dumps(area_json),
            utility_json=json.dumps(utility_json),
            SYNTHETIC_STEP=SYNTHETIC_STEP)

    if request.method == 'HEAD':
        return 'OK'


@visualize_blueprint.route('/p2/', methods=['GET', 'HEAD'])
def p2():
    """P2, perfromance report."""

    if request.method == 'GET':
        filename = functools.reduce(
            os.path.join, [
                os.path.dirname(os.path.dirname(os.getcwd())),
                'data', 'exlog', 'log.json'])

        with open(filename) as f:
            content = f.readlines()
            f.close()

        content = [json.loads(it.strip()) for it in content]

        info = [it.get('info') for it in content]

        # distance-velocity
        dv_list = utils.rndVelocity(
            utils.positions2dv(info), 3)
        dv_list, dv_zeros = utils.sumCategories(
            dv_list, ['distance', 'velocity'], 2)

        # time-veolocity
        tv_list = utils.rndVelocity(
            utils.positions2tv(info), 3)
        tv_list, tv_zeros = utils.sumCategories(
            tv_list, ['time', 'velocity'], 2)
        tv_zeros.append([
            '>0',
            pd.DataFrame(
                tv_list,
                columns=['time', 'velocity'])['time'].sum()])

        # stop event
        stops = utils.filterByStopEvent(info)

        return render_template(
            'oper_p2.html',
            dv_list=dv_list,
            tv_list=tv_list,
            tv_zeros=tv_zeros,
            stops=stops,
            SYNTHETIC_STEP=SYNTHETIC_STEP)

    if request.method == 'HEAD':
        return 'OK'


@visualize_blueprint.route('/api/log/', methods=['POST', 'GET'])
def log():

    if request.method == 'POST':
        filename = functools.reduce(
            os.path.join, [
                os.path.dirname(os.path.dirname(os.getcwd())),
                'data', 'exlog', 'log.json'])

        if not os.path.exists(filename):
            with open(filename, mode='w') as f:
                f.write(json.dumps(request.json) + '\n')
        else:
            with open(filename, mode='a') as f:
                f.write(json.dumps(request.json) + '\n')
        return jsonify(request.json)

    if request.method == 'GET':
        filename = functools.reduce(
            os.path.join, [
                os.path.dirname(os.path.dirname(os.getcwd())),
                'data', 'exlog', 'log.json'])

        with open(filename) as f:
            content = f.readlines()
            content = [json.loads(it.strip()) for it in content]
        return jsonify(content)

    if request.method == 'HEAD':
        return 'OK'
