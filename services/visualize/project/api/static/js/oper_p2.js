// ==========================================
// load vector layers, for an area and utility
// ==========================================
var area_json = $('#area_json').data()['name'];
var vectorAreaSource = new ol.source.Vector({
  features: (new ol.format.GeoJSON()).readFeatures( area_json )
});
var vectorArea = new ol.layer.Vector({
  source: vectorAreaSource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(192, 192, 192, 1.0)',
      width: 3
    })
  })
});

var utility_json = $('#utility_json').data()['name'];
var vectorUnderSource = new ol.source.Vector({
  features: (new ol.format.GeoJSON()).readFeatures( utility_json )
});
var vectorUnder = new ol.layer.Vector({
  source: vectorUnderSource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(0, 0, 0, 1)',
      width: 3
    })
  })
});


var actual_json = $('#actual_json').data()['name'];
var vectorActualSource = new ol.source.Vector({
  features: (new ol.format.GeoJSON()).readFeatures( actual_json )
});
var vectorActual = new ol.layer.Vector({
  source: vectorActualSource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(255, 0, 0, 1)',
      width: 3
    })
  })
});


var optimal_json = $('#optimal_json').data()['name'];
var vectorOptimalSource = new ol.source.Vector({
  features: (new ol.format.GeoJSON()).readFeatures( optimal_json )
});
var vectorOptimal = new ol.layer.Vector({
  source: vectorOptimalSource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(0, 255, 0, 1)',
      width: 3
    })
  })
});


var fill = new ol.style.Fill();
var style = new ol.style.Style({
  fill: fill,
  stroke: new ol.style.Stroke({
    color: '#FFF',
    width: 1
  })
});
var distanceStyle = new ol.style.Style({
  fill: fill,
  stroke: new ol.style.Stroke({
    color: '#000',
    width: 1
  })
});

// ==========================================
// create the map
// ==========================================
// reZoom
var zoomTo = 17;
var reZoom = document.getElementById('reZoom');
reZoom.onclick = function() {
  if ( zoomTo != 17 ) {
    zoomTo = 17;
  } else {
    zoomTo = 21;
  }
  view.setZoom(zoomTo);
}


var scaleLineControl = new ol.control.ScaleLine();
// creating the view
var view = new ol.View({
  center: [480443.02,238333.78],
  zoom: 17
});

function onChange() {
  scaleLineControl.setUnits('metric');
}
onChange();


var map = new ol.Map({
  layers: [vectorArea, vectorUnder, vectorActual, vectorOptimal],
  target: 'map',
  controls: ol.control.defaults({
    attributionOptions: {
      collapsible: false
    }
  }).extend([scaleLineControl]),
  view: view
});
