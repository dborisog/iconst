// ==========================================
// load vector layers, for an area and utility
// ==========================================
var vArea = vectorSource('area_json', 'rgba(192, 192, 192, 1.0)');
var vUnder = vectorSource('utility_json', 'rgba(0, 0, 0, 0.1)');


var url_wfs_utilitygpr = 'http://localhost/geoserver/gis/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&typeName=gis:utilitygpr&' +
    'outputFormat=application/json&srsname=EPSG:27700';
var vectorGPRsource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function(extent) {return url_wfs_utilitygpr;},
    strategy: ol.loadingstrategy.bbox
});

var vectorGPR = new ol.layer.Vector({
  source: vectorGPRsource,
  style: new ol.style.Style({
    image: new ol.style.Circle({
      radius: 5,
      fill: new ol.style.Fill({color: '#666666'}),
      stroke: new ol.style.Stroke({color: '#bada55', width: 1})
    })
  })
});


// ==========================================
// animation
// ==========================================
var distanceStyle = new ol.style.Style({
  fill: (new ol.style.Fill()),
  stroke: new ol.style.Stroke({
    color: '#000',
    width: 1
  })
});


// ==========================================
// create the map
// ==========================================
// reZoom
var zoomTo = 17;
var reZoom = document.getElementById('reZoom');
reZoom.onclick = function() {
  if ( zoomTo != 17 ) {
    zoomTo = 17;
  } else {
    zoomTo = 21;
  }
  view.setZoom(zoomTo);
}


// creating the view
var view = new ol.View({
  center: [480443.02,238333.78],
  zoom: 17
});


var map = new ol.Map({
  layers: [vArea.layer, vUnder.layer, vectorGPR],
  target: 'map',
  controls: ol.control.defaults({
    attributionOptions: {
      collapsible: false
    }
  }).extend([( new ol.control.ScaleLine())]),
  view: view
});


// Geolocation marker
var marker = new ol.Overlay({
  positioning: 'center-center',
  element: document.getElementById('marker'),
  stopEvent: false
});
map.addOverlay(marker);


// ==========================================
// animation
// ==========================================

map.on('postcompose', function(evt) {
  var vectorContext = evt.vectorContext;
  vectorContext.setStyle(distanceStyle);

  if (line3 !== null) {
    vectorContext.drawGeometry(line3);
  }  
});


var interval = synstep;  // 1000 = 1 second, 3000 = 3 seconds
var excPosition;
function doAjax() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost/etrack/api/position/',
        data: $(this).serialize(),
        dataType: 'json',
        success: function (data) {
          //$('#hidden').val(data);// first set the value  
          excPosition = data;    
        },
        complete: function (data) {
            // Schedule the next
            setTimeout(doAjax, interval);
            // console.log(data);
            projecessExcPosition(excPosition, 'e3');
        }
    });

    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'http://localhost/udetect/api/location/',
        data:  JSON.stringify(excPosition),
        dataType: 'json',
        success: function(data) {
        }
    });

    toPost = {
      "meta": {
        "interval": interval,
        "scenario": "e3"
      },
      "info": {
          "timevalue": Date.now(),
          "position": excPosition,
          "reaction": {"stop": false}}
    }

    if ( excPosition ) {
      $.ajax({
          type: 'POST',
          contentType: "application/json; charset=utf-8",
          url: 'http://localhost/api/log/',
          data:  JSON.stringify(toPost),
          dataType: 'json',
          success: function(data) {
          }
      });
    }
}
doAjax();
