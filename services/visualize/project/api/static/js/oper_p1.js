// ==========================================
// load vector layers, for an area and utility
// ==========================================
var vArea = vectorSource('area_json', 'rgba(192, 192, 192, 1.0)');
var vUnder = vectorSource('utility_json', 'rgba(0, 0, 0, 1.0)');
var vBuffer = vectorBuffer('utility_json', 0.5);


// ==========================================
// create the map
// ==========================================
// reZoom
var zoomTo = 17;
var reZoom = document.getElementById('reZoom');
reZoom.onclick = function() {
  if ( zoomTo != 17 ) {
    zoomTo = 17;
  } else {
    zoomTo = 21;
  }
  view.setZoom(zoomTo);
}


// creating the view
var view = new ol.View({
  center: [480443.02,238333.78],
  zoom: 17
});


var map = new ol.Map({
  layers: [vArea.layer, vBuffer, vUnder.layer],
  target: 'map',
  controls: ol.control.defaults({
    attributionOptions: {
      collapsible: false
    }
  }).extend([(new ol.control.ScaleLine())]),
  view: view
});


// ==========================================
// animation
// ==========================================
var distanceStyle = new ol.style.Style({
  fill: (new ol.style.Fill()),
  stroke: new ol.style.Stroke({
    color: '#000',
    width: 1
  })
});

map.on('postcompose', function(evt) {
  var vectorContext = evt.vectorContext;
  vectorContext.setStyle(distanceStyle);

  if (point !== null) {
    vectorContext.drawGeometry(point);
  }
  if (line !== null) {
    vectorContext.drawGeometry(line);
  }  
  if (line2 !== null) {
    vectorContext.drawGeometry(line2);
  }    
  // if (line3 !== null) {
  //   vectorContext.drawGeometry(line3);
  // }  
});


// Geolocation marker
var marker = new ol.Overlay({
  positioning: 'center-center',
  element: document.getElementById('marker'),
  stopEvent: false
});
map.addOverlay(marker);


var interval = synstep;  // 1000 = 1 second, 3000 = 3 seconds
var excPosition;
var iReact;
var stopValue = false;
var toPost;
function simulatePositionChange(jsitem) {
    if ( stopValue === true ) {
      alert("Stop the excavator!");
    }

    projecessExcPosition(jsitem.position);
    stopValue = jsitem.reaction['stop'];
}


var jsdata, data, jsmeta, jsinfo;
var simulateMovement = function() {
  item = jsdata.shift();
  simulatePositionChange(item.info);
  setTimeout(simulateMovement, item.meta.interval);
}


$.ajax({
  type: 'GET',
  url: 'http://localhost/api/log/',
  data: data,
  async: false,
  dataType: 'json',
  success: function (data) {
    jsdata = data;
  }
});

simulateMovement();
