// ==========================================
// load vector layers
// ==========================================

function vectorSource(name, color) {
  var src_json = $('#'+name).data()['name'];
  var vectorSource = new ol.source.Vector({
    features: (new ol.format.GeoJSON()).readFeatures( src_json )
  });
  var vectorLayer = new ol.layer.Vector({
    source: vectorSource,
    style: new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: color,
        width: 3
      })
    })
  });
  return {'source': vectorSource, 'layer': vectorLayer};
}

// ==========================================
// build a buffer around a layer
// ==========================================

function vectorBuffer(name, buffer) {

  // set scale line buffer and config view
  var canvas = document.createElement('canvas');
  var pixelRatio = ol.has.DEVICE_PIXEL_RATIO;
  var context = canvas.getContext('2d');

  // Generate a canvasPattern with two circles on white background
  var pattern = (function() {
    canvas.width = 11 * pixelRatio;
    canvas.height = 11 * pixelRatio;
    // white background
    context.fillStyle = 'white';
    context.fillRect(0, 0, canvas.width, canvas.height);
    // outer circle
    context.fillStyle = 'rgba(126, 126, 126, 1)';
    context.beginPath();
    context.arc(5 * pixelRatio, 5 * pixelRatio, 4 * pixelRatio, 0, 2 * Math.PI);
    context.fill();
    // inner circle
    context.fillStyle = 'rgb(256, 256, 256)';
    context.beginPath();
    context.arc(5 * pixelRatio, 5 * pixelRatio, 3 * pixelRatio, 0, 2 * Math.PI);
    context.fill();
    return context.createPattern(canvas, 'repeat');
  }());

  // Generate style for gradient or pattern fill
  var fill = new ol.style.Fill();
  var style = new ol.style.Style({
    fill: fill,
    stroke: new ol.style.Stroke({
      color: '#FFF',
      width: 1
    })
  });

  var getStackedStyle = function(feature, resolution) {
    fill.setColor(pattern);
    return style;
  };

  var source = new ol.source.Vector();
  var src_json = $('#'+name).data()['name'];
  var features = (new ol.format.GeoJSON()).readFeatures( src_json );
  var parser = new jsts.io.OL3Parser();

  for (var i = 0; i < features.length; i++) {
    var feature = features[i];
    // convert the OpenLayers geometry to a JSTS geometry
    var jstsGeom = parser.read(feature.getGeometry());

    // create a buffer of 40 meters around each line
    var buffered = jstsGeom.buffer(buffer);

    // convert back from JSTS and replace the geometry on the feature
    feature.setGeometry(parser.write(buffered));
  }
  source.addFeatures(features);


  var vectorLayer = new ol.layer.Vector({
    source: source,
    style: getStackedStyle
  });

  return vectorLayer;
}


// ==========================================
// shortest and bearing distances
// ==========================================

// recenters the view by putting the given coordinates at 3/4 from the top or the screen
function getCenterWithHeading(position, rotation, resolution) {
  var size = map.getSize();
  var height = size[1];

  return [
    position[0] - Math.sin(rotation) * height * resolution * 1 / 4,
    position[1] + Math.cos(rotation) * height * resolution * 1 / 4
  ];
}


// ==========================================
// shortest and bearing distances
// ==========================================

var point = null;
var line = null;
var wgs84Sphere= new ol.Sphere(6378137);

// shortest distance
var calcShortestDistance = function(coords) {
  // calcualte shortest distance between a ponit
  // and the closest relevant feature 

  var point_coords = new ol.geom.Point([coords.longitude, coords.latitude]);
  coordinate = point_coords.getCoordinates()

  var closestFeature = vUnder.source.getClosestFeatureToCoordinate(coordinate);
  if (closestFeature === null) {
    point = null;
    line = null;
  } else {
    var geometry = closestFeature.getGeometry();
    var closestPoint = geometry.getClosestPoint(coordinate);

    if (line === null) {
      line = new ol.geom.LineString([coordinate, closestPoint]);
      point = new ol.geom.Point(coordinate);
    } else {
      line.setCoordinates([coordinate, closestPoint]);
      point.setCoordinates(coordinate);
    }
  }

  return wgs84Sphere.haversineDistance(
    ol.proj.toLonLat(coordinate), 
    ol.proj.toLonLat(closestPoint));
};


var line_intersect = function(first, second) {
  // Returns point of intersection between two lines
  // Input:  an array of start and end coodinates for each line
  //         [[x1, y1], [x2, y2]] and [[x3, y3], [x4, y4]]
  // Output: [x5, y5], zeros if no intersection

    x1 = first[0][0];
    y1 = first[0][1];
    x2 = first[1][0];
    y2 = first[1][1];
    x3 = second[0][0];
    y3 = second[0][1];
    x4 = second[1][0];
    y4 = second[1][1];

    var ua, ub, denom = (y4 - y3)*(x2 - x1) - (x4 - x3)*(y2 - y1);
    if (denom == 0) {
        return null;
    }
    ua = ((x4 - x3)*(y1 - y3) - (y4 - y3)*(x1 - x3))/denom;
    ub = ((x2 - x1)*(y1 - y3) - (y2 - y1)*(x1 - x3))/denom;
    return {
        lon: x1 + ua*(x2 - x1),
        lat: y1 + ua*(y2 - y1),
        seg1: ua >= 0 && ua <= 1,
        seg2: ub >= 0 && ub <= 1
    };
}


var line2 = null;
var line3 = null
var calcBearingDistance = function(coords) {
  // calcualte bearing distance between a ponit and the closest relevant feature, 
  // returns 0 if no feature in the bearing

  var xy_here = [coords.longitude, coords.latitude];
  var angle = coords.heading;
  var vector = 700;
  var xy_far = [
    xy_here[0] + vector * Math.sin(angle),
    xy_here[1] + vector * Math.cos(angle)];

  brng_vector = [xy_here, xy_far];

  // if (line3 === null) {
  //   line3 = new ol.geom.LineString(brng_vector);
  // } else {
  //   line3.setCoordinates(brng_vector);
  // }

  coord_array = [];
  vUnder.source.forEachFeature(function(feature) {
    // console.log(feature);
    if (feature.getGeometry().getType() === "MultiLineString" ||
        feature.getGeometry().getType() === "LineString") {
      tmp_arr = feature.getGeometry().getCoordinates();
      i = 0;
      while (i < (tmp_arr[0].length - 1)) {
        coord_array.push([tmp_arr[0][i], tmp_arr[0][i+1]]);
        i++;
      }
    }
  });

  // check if any intersection in any features
  // http://paulbourke.net/geometry/pointlineplane/
  // Intersection point of two line segments in 2 dimensions
  intersection = [0, 0];
  coord_array.forEach(function(element) {
    tmpinter = line_intersect(brng_vector, element);
    if (tmpinter.seg1 === true && tmpinter.seg2 === true) {
      intersection = [brng_vector[0], [tmpinter.lon, tmpinter.lat]];
    }
  });

  // calculate distance to the intersections
  // find the shortest intersection
  // build a line between the current and the intersection.
  if (intersection[0] != 0 ){
    if (line2 === null) {
      line2 = new ol.geom.LineString(intersection);
      line2.extent_ = [
        intersection[0][0], intersection[0][1], intersection[1][0], intersection[1][1]];
      line2.extentRevision_ = 4;
    } else {
      line2.setCoordinates(intersection);
      line2.extent_ = [
        intersection[0][0], intersection[0][1], intersection[1][0], intersection[1][1]];
      line2.extentRevision_ = 4;
    }
      /*b_distance = Math.sqrt(
      (intersection[0][0] - intersection[1][0])^2 +
      (intersection[0][1] - intersection[1][1])^2);*/

      b_distance = wgs84Sphere.haversineDistance(
        ol.proj.toLonLat(intersection[0]), 
        ol.proj.toLonLat(intersection[1]));

    return b_distance;
  } else {
    line2 = null;
    return 9001;
  }
};


var calcBearingVector = function(coords) {
  var xy_here = [coords.longitude, coords.latitude];
  var angle = coords.heading;
  var vector = 700;
  var xy_far = [
    xy_here[0] + vector * Math.sin(angle),
    xy_here[1] + vector * Math.cos(angle)];

  brng_vector = [xy_here, xy_far];

  // if (line3 === null) {
  //   line3 = new ol.geom.LineString(brng_vector);
  // } else {
  //   line3.setCoordinates(brng_vector);
  // }
}

// audio signal
var audioSignal = document.getElementById("audioSignal"); 
var audioPlayed = false;

function projecessExcPosition(position, scenario) {
  exc_position = {
    'longitude': position.geometry.coordinates[0],
    'latitude': position.geometry.coordinates[1],
    'heading': position.properties.bearing,
    'velocity': position.properties.velocity
  }
  shrt_distance = calcShortestDistance(exc_position);
  brng_distance = calcBearingDistance(exc_position);

  if ( scenario === 'e3' ) {
    calcBearingVector(exc_position);
  }


  brng_angle = 0;
  if ( brng_distance != 9001 || brng_distance != 0 ) {
    brng_angle = Math.asin( shrt_distance / brng_distance );
  }

  view.setCenter(getCenterWithHeading(
      [exc_position.longitude, exc_position.latitude],
      -exc_position.heading,
      view.getResolution()));
  view.setRotation(-exc_position.heading);
  marker.setPosition([exc_position.longitude, exc_position.latitude]);

  map.render();


  if ( shrt_distance < 5 && audioPlayed === false ) {
    audioSignal.play();
    audioPlayed = true;
  }

  if ( shrt_distance > 5 && audioPlayed === true ) {
    audioPlayed = false;
  }

  addtohtml = '';
  if ( scenario != 'e3' ) {
    addtohtml = '<tr>' +
      '<td style="text-align:right">Shortest d.: </td>' +
      '<td>' + Math.round(shrt_distance * 100) / 100 + 'm' + '</td>' +
    '</tr>' +
    '<tr>' +
      '<td style="text-align:right">Heading d.: </td>' +
      '<td>' + Math.round(brng_distance * 100) / 100 + 'm' + '</td>' +
    '</tr>' +
    '<tr>' +
      '<td style="text-align:right">Angle: </td>' +
      '<td>' + Math.round((brng_angle * 180) / Math.PI, 0) + '&deg;</td>' +
    '</tr>'
  }

  var html = [
    '<table>' +
      '<tr>' +
        '<td style="text-align:right">X: </td>' +
        '<td>'+ exc_position.longitude + '</td>' +
      '</tr>' +
      '<tr>' +
        '<td style="text-align:right">Y: </td>' +
        '<td>'+ exc_position.latitude + '</td>' +
      '</tr>' +
      '<tr>' +
        '<td style="text-align:right">Velocity: </td>' +
        '<td>' + exc_position.velocity.toFixed(1) + ' km/h' + '</td>' +
      '</tr>' +
      '<tr>' +
        '<td style="text-align:right">Heading: </td>' +
        '<td>' + Math.round((exc_position.heading * 180) / Math.PI, 0) + '&deg;</td>' +
      '</tr>' +
      addtohtml +
    '</table>'];
    document.getElementById('info').innerHTML = html;
}
