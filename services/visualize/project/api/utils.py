# services/visualize/project/api/utils.py

import numpy as np
import pandas as pd
import random
import requests
from shapely import geometry


def fetch_json(url):
    """Return GeoJSON from GeoNode to emulate an underground utility.

    Input:
        url -- a HTTP url string.
    Output:
        output -- a GeoJSON string.
    """
    output = requests.get(url, auth=("admin", "geoserver"))
    return output.json()


def ppDistance(first, second):
    """Return distance between two points.

    Input:
        first: XY coordinates of the first point.
        second: XY coordinates of the second point.
    Output
        distance: the distance between two points.
    """

    distance = geometry.Point(first).distance(geometry.Point(second))
    return distance


def multipointDistance(pointarray):
    """Return accumulative distance between multiple points.

    Input:
        pointarray: an array of XY points
    Output
        distance: the distance between two points.
    """
    distance = 0
    for itr in range(1, len(pointarray)):
        distance += ppDistance(pointarray[itr-1], pointarray[itr])

    return distance


def positions2dv(pointarray):
    """Return distance and velocity recordings.

    Input:
        positions: an array of XY points
    Output
        dv_list: a list of [distance, velocity]
    """

    xyv_list = [{
        'coords': it.get('position').get('geometry').get('coordinates'),
        'velocity': it.get('position').get('properties').get('velocity')}
        for it in pointarray]

    dv_list = []
    for itr in range(1, len(xyv_list)):
        dv_list.append([
            round(ppDistance(
                xyv_list[itr-1].get('coords'),
                xyv_list[itr].get('coords')), 2),
            xyv_list[itr-1].get('velocity')])

    return dv_list


def positions2tv(pointarray):
    """Return distance and velocity recordings.

    Input:
        positions: an array of XY points
    Output
        dv_list: a list of [time, velocity]
    """

    tv_list = [{
        'time': it.get('timevalue'),
        'velocity': it.get('position').get('properties').get('velocity')}
        for it in pointarray]

    dtv_list = []
    for itr in range(1, len(tv_list)):
        dtv_list.append([
            (tv_list[itr].get('time') - tv_list[itr-1].get('time'))/1000,
            tv_list[itr-1].get('velocity')])

    return dtv_list


def rndVelocity(datalist, rndval):
    """Return randomized velocity, for demonstration purposes.

    Input:
        datalist: either dv_list or tv_list, see positions2dv & positions2tv
        rndval: the random value to change the velocity
    Output:
        output: a modified dv_list or tv_list
    """
    output = [
        [it[0], it[1] + random.randrange(-rndval, rndval, 1)]
        for it in datalist]

    return output


def sumCategories(datalist, columns, step):
    """Return a summarized by categories.

    Input:
        datalist: a list of lists, e.g. [[distance, velocity], ]
        columns: a list of variable names. e.g. ['distance', 'velocity']
        step: a step for velocity range
    Output:
        sumlist: a list of lists, e.g. [[distance, velocity], ]
    """
    sumlist = pd.DataFrame(datalist, columns=columns)

    extra_df = pd.DataFrame([[0, 0]], columns=columns)
    sumlist = sumlist.append(extra_df, ignore_index=True)

    zeros = sumlist[sumlist[columns[1]] == 0]
    sumlist = sumlist[sumlist[columns[1]] != 0]

    sumlist = sumlist.sort_values(by=columns[1])

    min_value = sumlist[columns[1]].min()
    max_value = sumlist[columns[1]].max()

    bins = list(range(0, int(max_value) + 1, step))
    ind = np.digitize(sumlist[columns[1]], bins)

    unqind = np.unique(ind).tolist()
    min_ind = min(unqind)
    max_ind = max(unqind)
    rnames = ['['+str(it)+', '+str(it+2)+')' for it in bins]
    rnames2 = []
    for it in rnames:
        if it[:4] == '[0, ':
            rnames2.append('(0, ' + it[4:])
        else:
            rnames2.append(it)
    ind_range = list(range(1, max_ind+1, 1))

    rnames = pd.DataFrame({'ind': ind_range, 'rnames': rnames2})
    sumlist['ind'] = ind
    allist = sumlist.merge(rnames, how='inner', left_on='ind', right_on='ind')

    allist.drop(['ind', columns[1]], axis=1, inplace=True)
    allist.columns = columns

    sumlist = allist.groupby(columns[1], as_index=False).sum()
    sumlist['cat'] = pd.Categorical(
        sumlist[columns[1]],
        categories=rnames2,
        ordered=True)

    sumlist.sort_values(by='cat', inplace=True)
    sumlist = sumlist[columns]

    sumlist = sumlist.values.tolist()

    zeros = zeros.groupby(columns[1], as_index=False).sum()
    zeros = zeros.values.tolist()

    return sumlist, zeros


def filterByStopEvent(infoarray):
    """Return pointarray filtered by Stop Reaction Event.

    Input:
        infoarray: an INFO part of the log
    Output:
        output: a filtered INFO part of the log
    """

    output = [
        it for it in infoarray if it.get('reaction').get('stop') is True]

    return output
