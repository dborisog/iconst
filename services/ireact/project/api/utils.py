# services/ireact/project/api/utils.py
# additional functions of ireact app

import requests
from shapely import geometry


def fetch_json(url):
    """Return GeoJSON for a given url.

    Input:
        url -- a HTTP url string.
    Output:
        output -- a GeoJSON string.
    """

    output = requests.get(url)
    return output.json()


def geojson2shapely(geovector):
    """Return Shapely from GeoJSON.

    Input:
        vector: a GeoJSON
    Output:
        geom: Shapely geomety object
    """

    if geovector.get('type') == 'Feature':
        geom = geometry.shape(geovector.get('geometry'))
        accuracy = geovector.get('properties').get('accuracy')

        return {'geometry': geom, 'properties': {'accuracy': accuracy}}

    if geovector.get("type") == 'FeatureCollection':
        final = []
        for feature in geovector.get('features'):
            coords = feature.get('geometry').get('coordinates')
            accuracy = feature.get('properties').get('accuracy')
            final.extend(coords)
        geom = geometry.MultiLineString(final)
        return {'geometry': geom, 'properties': {'accuracy': accuracy}}
    else:
        geom = accuracy = 0
        return {'geometry': geom, 'properties': {'accuracy': accuracy}}


def utility_state(vector, position, vector_buffer, position_buffer):
    """Return the utility reach state for the given position.

    Input:
        vector: an underground utility as a dict of Shapely geometry object and
                accuracy
        position:  position of the excavator as a dict of Shapely geometry
                object and accuracy
        vector_buffer: the inaccuracy buffer for the vector
        position_buffer: the inaccuracy buffer for the excavator position

    Output:
        output: a boolean, True if utility is present
    """

    vector_polygon = vector.get(
        'geometry').buffer(vector.get('properties').get('accuracy'))
    position_polygon = position.get(
        'geometry').buffer(position.get('properties').get('accuracy'))

    output = vector_polygon.intersects(position_polygon)

    return output
