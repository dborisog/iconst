# services/ireact/project/api/views.py
# views of GPS microservices

from flask import Blueprint, jsonify, render_template, request
from flask_cors import CORS
import os
from project.api import utils
import requests
from werkzeug.contrib.cache import SimpleCache


ireact_blueprint = Blueprint('ireact', __name__)
CORS(ireact_blueprint)


@ireact_blueprint.route('/api/rfid/', methods=['GET', 'HEAD'])
def rfid():
    """Receive and process stop signal..
    Temporal for synthetic data."""

    if request.method == 'GET':
        r = requests.get('http://wdetect:5000/api/people/')
        return jsonify(r.json())

    if request.method == 'HEAD':
        return 'OK'


@ireact_blueprint.route('/api/overhead/', methods=['GET', 'HEAD'])
def overhead():
    """Process overhead cable logics."""

    if request.method == 'GET':
        cache_obj = SimpleCache()

        position_url = 'http://etrack:5000/api/position/'
        machine_json = utils.fetch_json(position_url)

        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        if not cache_obj.get(utility_url):
            utility_json = utils.fetch_json(utility_url)
            cache_obj.set(utility_url, utility_json, timeout=60 * 15)
        else:
            utility_json = cache_obj.get(utility_url)

        shapely_utility = utils.geojson2shapely(utility_json)
        shapely_machine = utils.geojson2shapely(machine_json)

        # state = utils.overhead_state(shapely_utility, shapely_machine)
        state = utils.utility_state(shapely_utility, shapely_machine, 6.1, 4)

        if state:
            return jsonify({"stop": True})
        else:
            return jsonify({"stop": False})

    if request.method == 'HEAD':
        return 'OK'
