# services/ireact/project/tests/test_utils.py

import json
import os
from shapely import geometry
import unittest
from unittest import mock

from project.api import utils
from project.tests.base import BaseTestCase


# this methid will be used by the mock to replace requests.get
filename = os.path.join(os.path.dirname(__file__), 'undergroundvector.json')
with open(filename) as data_file:
    underground_geojson = json.load(data_file)
position_geojson = {
    "geometry": {
        "coordinates": [480454.334652, 238431.091757],
        "type": "Point"},
    "properties": {
        "bearing": 148.50035728270984, "velocity": 7,
        'accuracy': 0.2},
    "type": "Feature"}


def mocker_requests_get_json(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    if args[0] == 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name')):
        return MockResponse(underground_geojson, 200)
    elif args[0] == 'http://etrack:5000/api/position/':
        return MockResponse(
            {"geometry": {
                "coordinates": [480454.334652, 238431.091757],
                "type": "Point"},
                "properties": {
                    "bearing": 148.50035728270984, "velocity": 7,
                    'accuracy': 0.2},
                "type": "Feature"},
            200)

    return MockResponse(None, 404)


def mocker_requests_post_xml(*args, **kwargs):
    class MockResponse:
        def __init__(self, status_code):
            self.status_code = status_code

    if args[0] == 'http://geoserver:8080/geoserver/geonode/wfs':
        return MockResponse(200)

    return MockResponse(404)


class TestIReactUtils(BaseTestCase):
    """Test transactional utils of ireact service."""

    @mock.patch('requests.get', side_effect=mocker_requests_get_json)
    def test_fetch_json__scenarios__accurate(self, mock_get):
        # Test fetch_json.
        utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
        json_data = utils.fetch_json(utility_url)
        self.assertEqual(json_data, underground_geojson)

        json_data = utils.fetch_json('http://geoserver:8080/geoserver/')
        self.assertIsNone(json_data)

        position_url = 'http://etrack:5000/api/position/'
        json_data = utils.fetch_json(position_url)
        self.assertEqual(json_data, position_geojson)

    def test_geojson2shapely__sceanrios__accurate(self):
        # Test geojson2shapely

        point_obj = geometry.Point(480454.334652, 238431.091757)
        point_accuracy = 0.2
        self.assertEqual(
            {'geometry': point_obj,
             'properties': {'accuracy': point_accuracy}},
            utils.geojson2shapely(position_geojson))

        multiline_obj = geometry.MultiLineString([
          [
            [480348, 238569.6],
            [480313.91, 238627.86],
            [480310.52, 238689.43]
          ], [
            [480311.32, 238695.5],
            [480315.83158546, 238708.11784164]
          ], [
            [480355.09, 238555.26],
            [480349.4, 238570.3]
          ], [
            [480443.02, 238333.78],
            [480371.1, 238518.4],
            [480357.57, 238548.93]]])
        multiline_accuracy = 0.05

        self.assertEqual(
            {'geometry': multiline_obj,
             'properties': {'accuracy': multiline_accuracy}},
            utils.geojson2shapely(underground_geojson))

        self.assertEqual(
            {'geometry': 0,
             'properties': {'accuracy': 0}},
            utils.geojson2shapely({'type': 'nonfeature'}))

    def test_utility_state__scenarios__accurate(self):
        # test utlity_state()
        point_obj_true = geometry.Point(480357.57, 238548.93)
        point_obj_false = geometry.Point(380357.57, 238548.93)
        point_accuracy = 0.2

        multiline_obj = geometry.MultiLineString([
          [
            [480348, 238569.6],
            [480313.91, 238627.86],
            [480310.52, 238689.43]
          ], [
            [480311.32, 238695.5],
            [480315.83158546, 238708.11784164]
          ], [
            [480355.09, 238555.26],
            [480349.4, 238570.3]
          ], [
            [480443.02, 238333.78],
            [480371.1, 238518.4],
            [480357.57, 238548.93]]])
        multiline_accuracy = 0.05

        vector = {
            'geometry': multiline_obj,
            'properties': {'accuracy': multiline_accuracy}}
        position_true = {
            'geometry': point_obj_true,
            'properties': {'accuracy': point_accuracy}}
        position_false = {
            'geometry': point_obj_false,
            'properties': {'accuracy': point_accuracy}}
        vector_buffer = 6.1
        position_buffer = 4

        self.assertEqual(
            True,
            utils.utility_state(
                vector, position_true, vector_buffer, position_buffer))

        self.assertEqual(
            False,
            utils.utility_state(
                vector, position_false, vector_buffer, position_buffer))


if __name__ == '__main__':
    unittest.main()
