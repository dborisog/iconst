# services/udetect/project/tests/test_views.py

import json
import unittest

from project.tests.base import BaseTestCase


class TestUdetectService(BaseTestCase):
    """Tests for the udetect Service."""

    def test_location_json__HEAD__200status(self):
        # Ensure the /api/location route behaves correctly.
        response = self.client.head('/api/location/')
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
