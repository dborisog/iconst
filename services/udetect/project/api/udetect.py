# services/udetect/project/api/views.py
# views of GPS microservices

from flask import Blueprint, render_template, request
from flask_cors import CORS
import os

from project.api import utils
from werkzeug.contrib.cache import SimpleCache


udetect_blueprint = Blueprint(
    'udetect', __name__)
CORS(udetect_blueprint)


@udetect_blueprint.route('/api/location/', methods=['POST', 'GET', 'HEAD'])
def location():

    if request.method == 'HEAD':
        return 'OK'

    else:

        if request.json:

            cache_obj = SimpleCache()

            utility_url = 'http://geoserver:8080/geoserver/gis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gis:{}&outputFormat=application/json&srsname=EPSG:27700'.format(os.environ.get('underground_gis_name'))
            if not cache_obj.get(utility_url):
                utility_json = utils.fetch_json(utility_url)
                cache_obj.set(utility_url, utility_json, timeout=60 * 15)
            else:
                utility_json = cache_obj.get(utility_url)

            shapely_utility = utils.geojson2shapely(utility_json)
            shapely_location = utils.geojson2shapely(request.json)

            shapely_previous = utils.geojson2shapely(
                request.json.get('properties').get('previous'))

            shapely_step = utils.points2line(
                shapely_location, shapely_previous)

            state = utils.utility_state(
                shapely_utility, shapely_step, 0, 0)
            if state:
                (y, x) = utils.findUtility()

                url = 'http://geoserver:8080/geoserver/geonode/wfs'
                utils.push_pont2geoserver(
                    url, shapely_location)

            return "got json"
        else:
            return "no json received"
