# services/udetect/project/api/utils.py
# additional functions of util app

import numpy as np
import os
import pandas as pd
import requests
from shapely import geometry
from skimage.filters import threshold_otsu
from skimage.color import rgb2gray
import struct
import time


def fetch_json(url):
    """Return GeoJSON from GeoNode to emulate an underground utility.

    Input:
        url -- a HTTP url string.
    Output:
        output -- a GeoJSON string.
    """

    output = requests.get(url)
    return output.json()


# absolute minimum total header size
MINHEADSIZE = 1024


# def readgssi(infile):
#     # https://github.com/iannesbitt/readgssi/blob/master/readgssi.py
#     # read DZT file
#     with open(infile, 'rb') as f:
#         # open the binary, start reading chunks
#         # 0x00ff if header, 0xfnff if old file format
#         rh_tag = struct.unpack('<h', f.read(2))[0]
#         # offset to data from beginning of file
#         rh_data = struct.unpack('<h', f.read(2))[0]
#         # samples per scan
#         rh_nsamp = struct.unpack('<h', f.read(2))[0]
#         # bits per data word
#         rh_bits = struct.unpack('<h', f.read(2))[0]
#         # skip across some proprietary stuff
#         f.seek(44)
#         # number of channels
#         rh_nchan = struct.unpack('<h', f.read(2))[0]
#         # whether or not the header is normal or
#         # big-->determines offset to data array
#         if rh_data < MINHEADSIZE:
#             f.seek(MINHEADSIZE * rh_data)
#         else:
#             f.seek(MINHEADSIZE * rh_nchan)

#         # 8/16/32-bit
#         if rh_bits == 8:
#             data = np.fromfile(f, np.uint8).reshape(-1, rh_nsamp).T
#         elif rh_bits == 16:
#             data = np.fromfile(f, np.uint16).reshape(-1, rh_nsamp).T
#         else:
#             data = np.fromfile(f, np.int32).reshape(-1, rh_nsamp).T

#     # write to CSV
#     fnoext = os.path.splitext(infile)[0]
#     of = os.path.abspath(fnoext + '.csv')
#     data = pd.DataFrame(data)
#     data.to_csv(of)
#     del data


def readCSVsyntheticScenario():
    """Return numpy array from a sample file with encoded utility.
    It is a part of synthetic data processing scenario.

    Input:
    Output:
        sample: numpy array of the size 510 to 250, 250 being X coordinate.
    """

    csv_filepath = os.path.join(
        os.path.join(
            os.path.join(os.getcwd(), 'project'),'data'), 'simple50cm.csv')

    # sample = np.genfromtxt(csv_filepath, delimiter=',', dtype=int)
    sample = pd.read_csv(
        csv_filepath,
        sep=',',
        header='infer',
        index_col=0,
        skiprows=[1])

    return sample


def findUtility():
    """Return depth and position on the line, from a semi-synthetic GPR file.

    Input:
    Output:
        (y, x) tuple with int indices of the utility on image array
    """

    # this parameter determines the shape of hyperbola
    a = b = 8
    surface = 88

    sample = readCSVsyntheticScenario()
    # Step 1: Convert the array to absolute values
    raw_ROI = sample.iloc[surface:,:]
    raw_ROI = raw_ROI.abs()

    # Step 2: Remove the mean of each horizontal line 
    # (for contrast improvement)
    raw_ROI = raw_ROI.sub(raw_ROI.mean(axis=1), axis=0)
    raw_ROI = raw_ROI.as_matrix()

    # Step 3: Select the region of interests 
    # (from top to the land surface is ignored, I used 100 pixels in y axis)
    # normilize to [0, 1], mat2gray() equivalent
    raw_ROI = (raw_ROI - np.min(raw_ROI))/np.ptp(raw_ROI)

    # Step 4: Binarize the image using Otsu’e threshold.
    # http://scikit-image.org/docs/dev/auto_examples/xx_applications/plot_thresholding.html

    # thresholf value: black and white image, 
    # which satisfies the threshold value
    image = raw_ROI
    # level = threshold_otsu(image)
    level = threshold_otsu(raw_ROI)
    tmp_ROI = raw_ROI > level

    (y, x) = np.nonzero(tmp_ROI)
    (sy, sx) = tmp_ROI.shape
    sx_range = np.arange(0, sx)
    totalpix = len(x)

    # Step 5: Apply Hough Transform to detect the hyperbola objects.

    HM = np.zeros(shape=(sy, sx), dtype=int)
    xo = np.array(range(0, sx))

    m_x = myt = np.zeros(shape=(x.shape[0], xo.shape[0]), dtype=int)
    m_x += x[:,np.newaxis]

    m_y = (
        y[:,np.newaxis] - 
        np.sqrt(np.multiply(a**2, np.divide(np.power(m_x - xo, 2), b**2) + 1))
    ).astype('int')

    for row in m_y:
        indid = np.where((row <= sy) & (row > 0))
        HM[row[indid], indid] += 1

    # Yo and Xo are the location of objects
    (Yo, Xo) = np.where(HM == HM.max())

    Object_y = int(np.mean(Yo) + a + surface)
    Object_x = int(np.mean(Xo))

    return (Object_y, Object_x)


def geojson2shapely(geovector):
    """Return Shapely from GeoJSON.

    Input:
        vector: a GeoJSON
    Output:
        geom: Shapely geomety object
    """

    if geovector.get('type') == 'Feature':
        geom = geometry.shape(geovector.get('geometry'))
        accuracy = geovector.get('properties').get('accuracy')

        return {'geometry': geom, 'properties': {'accuracy': accuracy}}

    if geovector.get("type") == 'FeatureCollection':
        final = []
        for feature in geovector.get('features'):
            coords = feature.get('geometry').get('coordinates')
            accuracy = feature.get('properties').get('accuracy')
            final.extend(coords)
        geom = geometry.MultiLineString(final)
        return {'geometry': geom, 'properties': {'accuracy': accuracy}}
    else:
        geom = accuracy = 0
        return {'geometry': geom, 'properties': {'accuracy': accuracy}}


def points2line(current, previous):
    """Return Shapely MultilineString from two Shapely Points.

    Input:
        current: current position, Shapely point
        previous: previous position, Shapely point
    Output:
        line: the line between two points, Shapely MultiLineString.
    """

    geom = geometry.MultiLineString([(
        current.get('geometry').coords[:][0],
        previous.get('geometry').coords[:][0])])

    return  {
        'geometry': geom,
        'properties': {'accuracy': current.get('properties').get('accuracy')}}


def utility_state(vector, position, vector_buffer, position_buffer):
    """Return the utility reach state for the given position.

    Input:
        vector: an underground utility as a dict of Shapely geometry object and
                accuracy
        position:  position of the excavator as a dict of Shapely geometry
                object and accuracy
        vector_buffer: the inaccuracy buffer for the vector
        position_buffer: the inaccuracy buffer for the excavator position

    Output:
        output: a boolean, True if utility is present
    """

    vector_polygon = vector.get(
        'geometry').buffer(vector.get('properties').get('accuracy'))
    position_polygon = position.get(
        'geometry').buffer(position.get('properties').get('accuracy'))

    output = vector_polygon.intersects(position_polygon)

    return output


def push_pont2geoserver(url, feature):
    """Push a Point feature to GeoServer.

    Input:
        url: url for GeoServer WFS
        feature: a Shapely feature
    Output:
        r: satus_code of the response
    """

    url = 'http://geoserver:8080/geoserver/gis/wfs'
    xml = """
    <wfs:Transaction service="WFS" version="1.0.0"
      xmlns:wfs="http://www.opengis.net/wfs"
      xmlns:gml="http://www.opengis.net/gml">
      <wfs:Insert>
        <utilitygpr>
          <the_geom>
            <gml:Point srsName="http://www.opengis.net/gml/srs/epsg.xml#27700">
              <gml:coordinates decimal="." cs="," ts=" ">
                {},{}
              </gml:coordinates>
            </gml:Point>
          </the_geom>
          <accuracy>{}</accuracy>
        </utilitygpr>
      </wfs:Insert>
    </wfs:Transaction>""".format(
        feature.get('geometry').coords.xy[0][0],
        feature.get('geometry').coords.xy[1][0],
        feature.get('properties').get('accuracy'))
    headers = {'Content-Type': 'application/xml'}

    r = requests.post(
        url, data=xml, headers=headers, auth=("admin", "geoserver"))
    return r.status_code
