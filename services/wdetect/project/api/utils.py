# services/wdetect/project/api/utils.py
# additional functions of wdetect app

import requests
from shapely import geometry


def get_man_coords():
    """Get human coords.

    Input:
    Output:
        coords: an XY array pf EPSG:27700 values."""
    return [480441.16, 238296.69]


def fetch_json(url):
    """Return GeoJSON for a given url.

    Input:
        url -- a HTTP url string.
    Output:
        output -- a GeoJSON string.
    """

    output = requests.get(url)
    return output.json()


def get_rfid_distance():
    """Regturn RFID people detection distancce.

    Input:
    Output:
        RFID detection distance, in meters
    """

    return 2


def check_intersection(machine, man):
    """Return True if a man is discovered by RFID sensor.

    Input:
        machine: position of the excavator, GeoJSON
        man: position of a human, XY array, meters EPSG:27700.
    Output:
        intersect: True if a man in machine's RFID distance
    """

    rfid_distance = get_rfid_distance()

    machine_polygon = geometry.Point(machine.get(
            'geometry').get('coordinates')).buffer(rfid_distance)
    # it is assumed an RFID tag is 0.01 m big
    man_polygon = geometry.Point(man).buffer(0.01)

    intersects = machine_polygon.intersects(man_polygon)

    return intersects
