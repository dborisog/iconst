# services/wdetect/project/api/views.py
# views of GPS microservices

from flask import Blueprint, jsonify, render_template, request
from flask_cors import CORS
from . import utils


wdetect_blueprint = Blueprint(
    'wdetect', __name__)
CORS(wdetect_blueprint)


@wdetect_blueprint.route('/api/people/', methods=['GET', 'HEAD'])
def people():
    """Executes GET request if a man if detected by RFID sensor.
    Temporal for synthetic data."""

    if request.method == 'GET':
        position_url = 'http://etrack:5000/api/position/'
        machine_json = utils.fetch_json(position_url)

        man_coords = utils.get_man_coords()

        intersection = utils.check_intersection(machine_json, man_coords)

        if intersection:
            return jsonify({"stop": True})
        else:
            return jsonify({"stop": False})

    if request.method == 'HEAD':
        return 'OK'
