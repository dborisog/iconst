# services/wdetect/project/tests/test_utils.py

import unittest
from unittest import mock

from project.api import utils
from project.tests.base import BaseTestCase


position_geojson = {
    "geometry": {
        "coordinates": [480454.334652, 238431.091757],
        "type": "Point"},
    "properties": {
        "bearing": 148.50035728270984, "speed": 7,
        'accuracy': 0.2},
    "type": "Feature"}


def mocker_requests_get_json(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    if args[0] == 'http://etrack:5000/api/position/':
        return MockResponse(
            {"geometry": {
                "coordinates": [480454.334652, 238431.091757],
                "type": "Point"},
                "properties": {
                    "bearing": 148.50035728270984, "speed": 7,
                    'accuracy': 0.2},
                "type": "Feature"},
            200)


class TestUdetectUtils(BaseTestCase):
    """Test transactional utils of ireact service."""

    def test_get_man_coords__scenarios__accurate(self):
        # Test get_man_coords.
        man_coords = [480441.16, 238296.69]
        self.assertEqual(man_coords, utils.get_man_coords())

    @mock.patch('requests.get', side_effect=mocker_requests_get_json)
    def test_fetch_json__scenario__accurate(self, mock_get):
        # Test get_excavator_position.
        url = 'http://etrack:5000/api/position/'
        self.assertEqual(position_geojson, utils.fetch_json(url))

    def test_get_rfid_distance__scenario__accurate(self):
        # Test get_rfid_distance.
        self.assertEqual(2, utils.get_rfid_distance())

    def test_check_intersection__scenarios__accurate(self):
        # Test if stop_signal() works properly.

        nearby_man_coords = [480454.334652, 238431.091757]
        faraway_man_coords = [480241.16, 238296.69]

        self.assertEqual(True, utils.check_intersection(
            position_geojson, nearby_man_coords))

        self.assertEqual(False, utils.check_intersection(
            position_geojson, faraway_man_coords))


if __name__ == '__main__':
    unittest.main()
