# services/wdetect/project/tests/test_views.py

import unittest

from project.tests.base import BaseTestCase


class TestPositionService(BaseTestCase):
    """Tests for the iReact Service."""

    def test_people_json__HEAD__200status(self):
        # Ensure the / route behaves correctly.
        response = self.client.head('/api/people/')
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
